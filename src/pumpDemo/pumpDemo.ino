#define PUMP_DIR          9
#define PUMP_STEP         10
#define PUMP_CS           11
#define PUMP_EN           12

#define MINDELAY 1
#define MAXDELAY 100
#define DIFFDELAY 2

bool stp = false;
int cdelay = MAXDELAY;
void setup() {
  // put your setup code here, to run once:
  pinMode(PUMP_DIR,       OUTPUT);
  pinMode(PUMP_STEP,      OUTPUT);
  pinMode(PUMP_CS,        OUTPUT);
  pinMode(PUMP_EN,        OUTPUT);

  digitalWrite(PUMP_DIR, HIGH);
  digitalWrite(PUMP_CS, HIGH);
  digitalWrite(PUMP_EN, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  stp = !stp;
  digitalWrite(PUMP_STEP, stp);
  delay(cdelay);
  cdelay = cdelay - DIFFDELAY;
  if(cdelay < MINDELAY){
    cdelay = MINDELAY;
  }
}
