#define __DEBUG_MODE__ 1 //if 0, debug mode disabled, otherwise enabled (debug mode: send serial (115200 baud) debug message) 

#define ENABLE_SW         19
#define POTMETER          A1

#define LVL_SW_TOP        16    // the level switch in the top tank
#define LVL_SW_BOTTOM     17    // the level switch in the bottom tank

#define PUMP_DIR          9
#define PUMP_STEP         10
#define PUMP_CS           11
#define PUMP_EN           12

#define WATER_MOTOR       3     // the motor that fills the top tank with water
#define VALVE_0           4     // the valve upstream of the top tank
#define VALVE_1           5     // the valve between the top and bottom tank


#define PUMP_SPEED        100   // inverse speed: delay between steps
#define PUMP_STEPS_SCALE  16    // the number of pump motor steps per potmeter value
#define PUMP_BASE_STEPS   127   // pump motor steps when the potmeter is at 0
#define PUMP_DIRECTION    HIGH  // the direction of the motor pump rotation
#define TOP_TO_BOT_TIME   30000 // time it takes for the solution to flow from the top tank to the bottom tank

#define VALVE_OPEN        LOW   // value of the pin if the valve is open
#define LVL_SWITCH_ON     HIGH  // value of the pin when the switch is "on"
#define PUMP_ENABLED      LOW   // value of the PUMP_EN pin to enable the pump
#define MOTOR_ON          LOW   // value of the pin when the motor is switched on
#define ENABLE_SWITCH_ON  HIGH  // value of the enable switch pin when the switch is switched "on"


int potmeterValue = 0;
int pumpStepNum = 0;

void fillTopTank();
void pourTopToBottom();


void setup() {
  pinMode(ENABLE_SW,      INPUT);
  pinMode(POTMETER,       INPUT);
  
  pinMode(LVL_SW_TOP,     INPUT);
  pinMode(LVL_SW_BOTTOM,  INPUT);
  
  pinMode(PUMP_DIR,       OUTPUT);
  pinMode(PUMP_STEP,      OUTPUT);
  pinMode(PUMP_CS,        OUTPUT);
  pinMode(PUMP_EN,        OUTPUT);
  
  pinMode(WATER_MOTOR,    OUTPUT);
  pinMode(VALVE_0,        OUTPUT);
  pinMode(VALVE_1,        OUTPUT);


  digitalWrite(PUMP_DIR, PUMP_DIRECTION);
  digitalWrite(PUMP_CS, HIGH);
  digitalWrite(PUMP_STEP, LOW);
  digitalWrite(PUMP_EN, !PUMP_ENABLED);
  
  digitalWrite(WATER_MOTOR, !MOTOR_ON);
  digitalWrite(VALVE_0, !VALVE_OPEN);
  digitalWrite(VALVE_1, !VALVE_OPEN);
  pourTopToBottom();

#if __DEBUG_MODE__
  Serial.begin(115200);
#endif
}

void loop() {
  if((digitalRead(LVL_SW_BOTTOM) != LVL_SWITCH_ON) && (digitalRead(LVL_SW_TOP) == LVL_SWITCH_ON)){
    pourTopToBottom();
  }
  
#if __DEBUG_MODE__
  else {
    if(digitalRead(LVL_SW_BOTTOM) == LVL_SWITCH_ON) {
      Serial.println("0: bottom tank full");
    }
    if(digitalRead(LVL_SW_TOP) != LVL_SWITCH_ON) {
      Serial.println("1: top tank empty");
    }
  }
#endif

  if((digitalRead(LVL_SW_TOP) != LVL_SWITCH_ON) && (digitalRead(ENABLE_SW) == ENABLE_SWITCH_ON)){
    fillTopTank();
  }
  
#if __DEBUG_MODE__
  else {
    if(digitalRead(LVL_SW_TOP) == LVL_SWITCH_ON) {
      Serial.println("2: top tank full");
    }
    if(digitalRead(ENABLE_SW) != ENABLE_SWITCH_ON) {
      Serial.println("3: top refill disabled");
    }
  }
  delay(1000);
#endif
}

void fillTopTank() {
  if(digitalRead(LVL_SW_TOP) == LVL_SWITCH_ON){
#if __DEBUG_MODE__
    Serial.println("4: Uh-Oh, top tank is full - why was this called?");
#endif
    return;
  }
#if __DEBUG_MODE__
  Serial.println("5: Filling up top tank");
#endif
  potmeterValue = analogRead(POTMETER);
  pumpStepNum = PUMP_BASE_STEPS + potmeterValue * PUMP_STEPS_SCALE;
#if __DEBUG_MODE__
  Serial.print("6: pump motor steps:");
  Serial.println(pumpStepNum);
#endif
  digitalWrite(PUMP_EN, PUMP_ENABLED);

  // switch on water supply
  digitalWrite(VALVE_0, VALVE_OPEN);
  digitalWrite(WATER_MOTOR, MOTOR_ON);
#if __DEBUG_MODE__
  Serial.print("7: water supply on. Valve 0: ");
  Serial.print(VALVE_OPEN);
  Serial.print(", water motor: ");
  Serial.println(MOTOR_ON);
#endif

  // add from salt solution
  for(int ii=0;ii<pumpStepNum;ii++) {
    digitalWrite(PUMP_STEP, HIGH);
    delay(PUMP_SPEED);
    digitalWrite(PUMP_STEP, LOW);
    delay(PUMP_SPEED);
#if __DEBUG_MODE__
    Serial.println("B: pump stepped once");
#endif

    if(digitalRead(LVL_SW_TOP) == LVL_SWITCH_ON){
      // switch off water supply
      digitalWrite(VALVE_0, !VALVE_OPEN);
      digitalWrite(WATER_MOTOR, !MOTOR_ON);
    }
  }
  digitalWrite(PUMP_EN, !PUMP_ENABLED);
#if __DEBUG_MODE__
  Serial.println("8: pump off");
#endif 

  // wait for top tank to fill
  while(digitalRead(LVL_SW_TOP) != LVL_SWITCH_ON);
  // switch off water supply
  digitalWrite(VALVE_0, !VALVE_OPEN);
  digitalWrite(WATER_MOTOR, !MOTOR_ON);
}

void pourTopToBottom(){
  if(digitalRead(LVL_SW_BOTTOM) == LVL_SWITCH_ON){
#if __DEBUG_MODE__
  Serial.println("9: Uh-Oh: why was this called, bottom full");
#endif 
    return;
  }
#if __DEBUG_MODE__
  Serial.println("A: pouring top to bottom");
#endif 
  digitalWrite(VALVE_1, VALVE_OPEN);
  delay(TOP_TO_BOT_TIME);
  digitalWrite(VALVE_1, !VALVE_OPEN);
}

